# Schémas Frizting pour les cartes STM32 des kits pédagogiques

[Frizting](https://fritzing.org) est un environnement de dessin de montage électronique pédagogique.

Ce dépôt contient les bibliothèques Fritzing des composants et des cartes utilisés pour les tutoriels.

Il complète des bibliothèques déjà existantes :
* [Core](https://github.com/fritzing/fritzing-parts/tree/master/core)
* [Grove](https://github.com/Seeed-Studio/fritzing_parts)